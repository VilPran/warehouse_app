# Bakalaurinis_darbas

## Programos paleidimo instrukcija

Norint paleisti programą reikia atlikti šiuos veiksmus:

1. Nusiklonuoti repozitoriją:

```
git clone https://gitlab.com/VilPran/warehouse_app.git

```
2. Nueiti į git aplanką

```
cd warehouse_app

```

3. Išskleisti flask.zip failą. Windows aplinkoje naudojant pvz. 7-zip komandą, o Linux unzip komandą:

```
unzip flask.zip

```

4. Nueiti į programos aplanką:

```
cd flask

```

6. Aktyvuoti virtualią aplinką (venv):

jei naudojama Windows operacinė sistema:

```
venv\Scripts\activate

```
jei naudojama Linux operacinė sistema:

```
source venv/Scripts/activate

```

7. Instaliuoti reikalingas bibliotekas:

```
pip install -r requirements.txt

```
8. Paleisti python script'ą

```
python3 app.py

```

9. Paleidus aplikaciją terminale atsiranda nuoroda lokalųjį serverį: http://127.0.0.1:5000/sign-up/ Nukopijavus šią nuorodą ir įvedus ją naršyklės paieškos lange pateksite į registracijos langą, kuriame norėdami naudotis aplikacija turėsite susikurti savo paskyrą.
